TEMPLATE = subdirs

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

!isEmpty(HELP) {
    message("CONFIG flags: ")
    message(" By default skeyer_demo is built unless you choose to build maliit plugin or libskeyer")
    message("    build-maliit-plugin: builds Skeyer Maliit Plugin.")
    message("    build-standalone-library: builds libskeyer.")
    message("    build-skeyer-demo: builds skeyer the skeyer demo application")
    message("    build-tests: build unit tests")
    message("Important build options:")
    message("    PREFIX: Install prefix. default: ./install")
    message("Influential environment variables:")
    message("    QMAKEFEATURES A mkspecs/features directory list to look for features.")
    message("    Use it if a dependency is installed to non-default location.")
    message("Examples:")
    message("    qmake")
    message("    qmake CONFIG += build-maliit-plugin qmake PREFIX=/usr LIBDIR=/usr/lib64")
} else {
    message("Tip: Run qmake HELP=1 for a list of all supported build options")
}


build-maliit-plugin {
    SUBDIRS = maliit_plugin
}

build-standalone-library {
    SUBDIRS = lib
}

build-demo {
    SUBDIRS = demo
}

build-tests {
    SUBDIRS += tests
}

OTHER_FILES += run_demo.sh COPYING README TODO THANKS
