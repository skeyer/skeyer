#include "skeyerinputmethod.h"
#include "engine.h"
#include "skeyerview.h"

#include <maliit/plugins/abstractinputmethodhost.h>

#include <QDebug>
#include <QString>
#include <QQmlApplicationEngine>

using namespace Skeyer;

SkeyerInputMethod::SkeyerInputMethod(MAbstractInputMethodHost *host) :
    MAbstractInputMethod(host)
{
    m_view = new SkeyerView;
    m_view->setFlags(Qt::FramelessWindowHint);

    connect( m_view->wordEngine(), SIGNAL(wordSelected(QString,QString,QList<QPointF>)),
             this, SLOT(onWordSelected(QString,QString,QList<QPointF>)) );

    connect(m_view->wordEngine(), SIGNAL(currentWordChanged(QString)), SLOT(onSuggestionUpdated(QString)));

    connect(m_view, SIGNAL(keyboardGeometryChanged(QRect)), SLOT(keyboardGeometryChanged(QRect)));
    connect(m_view->engine(), SIGNAL(quit()), host, SLOT(notifyImInitiatedHiding())) ;

    host->registerWindow(m_view,Maliit::PositionCenterBottom);
    host->setScreenRegion( m_view->keyboardGeometry() );
}

SkeyerInputMethod::~SkeyerInputMethod()
{
    qDebug()<<"~SkeyerInputMethod";
    delete m_view;
}

void SkeyerInputMethod::show()
{
    m_view->showFullScreen();
    qDebug()<<"skeyer::show";
}

void SkeyerInputMethod::hide()
{
    m_view->hide();
    qDebug()<<"skeyer::hide";
}

void SkeyerInputMethod::handleVisualizationPriorityChange(bool priority)
{
    qDebug()<<"skeyer::handleVisualizationPriorityChange";
    if(priority) show();
    else hide();
}

void SkeyerInputMethod::handleClientChange()
{
    // Clients connect to Maliit on startup and disconnect at quit. This method is called
    // for both those events. It makes sense to hide the keyboard always on these events,
    // especially if the client crashes, so that the OSK is closed.
    hide();
    qDebug()<<"skeyer::handleClientChange";
}

void SkeyerInputMethod::handleAppOrientationAboutToChange(int angle)
{
    qDebug()<<"skeyer::handleAppOrientationAboutToChange";
}

void SkeyerInputMethod::handleAppOrientationChanged(int angle)
{
    qDebug()<<"skeyer::handleAppOrientationChanged";
}

QList<MAbstractInputMethod::MInputMethodSubView> SkeyerInputMethod::subViews(Maliit::HandlerState state) const
{
    Q_UNUSED(state)
    qDebug()<<"skeyer::subViews";
    QList<MAbstractInputMethod::MInputMethodSubView> subViews;
    MAbstractInputMethod::MInputMethodSubView subView1;
    subView1.subViewId = "skeyer.subview.1";
    subView1.subViewTitle = "Skeyer subview 1";
    subViews.append(subView1);
    return subViews;

}

void SkeyerInputMethod::setActiveSubView(const QString &subViewId, Maliit::HandlerState state)
{
    Q_UNUSED(state)
    Q_UNUSED(subViewId)
    qDebug()<<"skeyer::setActiveSubView";
}

QString SkeyerInputMethod::activeSubView(Maliit::HandlerState state) const
{
    Q_UNUSED(state)
    return "en_US";
}

void SkeyerInputMethod::setState(const QSet<Maliit::HandlerState> &state)
{
    if (state.contains(Maliit::OnScreen)) {
        show();
    } else {
        hide();
    }
    qDebug()<<"skeyer::setState";
}

void SkeyerInputMethod::update()
{
    qDebug()<<"skeyer::update";
}

void SkeyerInputMethod::reset()
{
    qDebug()<<"skeyer::reset";
}

void SkeyerInputMethod::switchContext(Maliit::SwitchDirection direction, bool enableAnimation)
{
    qDebug()<<"skeyer::switchContext";
}

void SkeyerInputMethod::handleFocusChange(bool focusIn)
{
    if(!focusIn) hide();
    else show();
    qDebug()<<"skeyer::handleFocusChange";
}

void SkeyerInputMethod::setPreedit(const QString &preeditString, int cursorPos)
{
    Q_UNUSED(preeditString)
    Q_UNUSED(cursorPos)
    qDebug()<<"skeyer::setPreedit";
}

void SkeyerInputMethod::onSuggestionUpdated(const QString word)
{
    Maliit::PreeditTextFormat format;
    format.start = 0;
    format.length = word.length();
    if (word.length() == 0) {
        format.preeditFace = Maliit::PreeditNoCandidates;
    } else {
        format.preeditFace = Maliit::PreeditActive;
    }
    inputMethodHost()->sendPreeditString(word, QList<Maliit::PreeditTextFormat>() << Maliit::PreeditTextFormat());
}

void SkeyerInputMethod::onWordSelected(const QString& word, const QString &input, const QList<QPointF> &inputPath)
{
    qDebug()<<"Word Selected:" << word;
    inputMethodHost()->sendCommitString(word);
    inputMethodHost()->sendCommitString(" ");
}

void SkeyerInputMethod::onBackspacePressed()
{
    inputMethodHost()->sendKeyEvent(QKeyEvent(QKeyEvent::KeyPress, Qt::Key_Backspace, Qt::NoModifier));
}

void SkeyerInputMethod::keyboardGeometryChanged(const QRect &geometry)
{
    inputMethodHost()->setScreenRegion(QRegion(geometry));
}
