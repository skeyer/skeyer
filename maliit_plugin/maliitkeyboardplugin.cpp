#include "maliitkeyboardplugin.h"
#include "skeyerinputmethod.h"

MaliitKeyboardPlugin::MaliitKeyboardPlugin(QObject *parent) :
    QObject(parent)
{
}

QString MaliitKeyboardPlugin::name() const
{
    return QString("Maliit Keyboard");
}

MAbstractInputMethod *MaliitKeyboardPlugin::createInputMethod(MAbstractInputMethodHost *host)
{
    return new SkeyerInputMethod(host);
}

QSet<Maliit::HandlerState> MaliitKeyboardPlugin::supportedStates() const
{
    QSet<Maliit::HandlerState> set;
    set.insert(Maliit::OnScreen);
    return set;
}
