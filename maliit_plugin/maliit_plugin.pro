TEMPLATE = lib
TARGET = skeyerplugin

QT = core gui widgets quick qml dbus
CONFIG += c++11

CONFIG += plugin

!load(maliit-defines) {
   error(Cannot find $$[QT_INSTALL_DATA]/mkspecs/features/maliit-defines.prf. Probably Maliit framework not installed)
}

# This enables the maliit library for C++ code
CONFIG += maliit-plugins

isEmpty(LIBDIR) {
   LIBDIR = $$PREFIX/lib
}

isEmpty(PREFIX) {
    PREFIX = $$MALIIT_PREFIX
    target.path += $${MALIIT_PLUGINS_DIR}
}
else {
    target.path += $$LIBDIR/maliit/plugins
}


include(../config.pri)
include(../view/view.pri)

INSTALLS += target

HEADERS += \
    maliitkeyboardplugin.h \
    skeyerinputmethod.h

SOURCES += \
    maliitkeyboardplugin.cpp \
    skeyerinputmethod.cpp
