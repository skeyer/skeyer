#ifndef MALIITKEYBOARDPLUGIN_H
#define MALIITKEYBOARDPLUGIN_H

#include <maliit/plugins/inputmethodplugin.h>
#include <QObject>

class MaliitKeyboardPlugin
    : public QObject, public Maliit::Plugins::InputMethodPlugin
{
    Q_OBJECT
    Q_INTERFACES(Maliit::Plugins::InputMethodPlugin)
#if QT_VERSION >= 0x050000
    Q_PLUGIN_METADATA(IID  "org.maliit.plugin.skeyer"
                      FILE "plugin.json")
#endif

public:
    explicit MaliitKeyboardPlugin(QObject *parent = 0);

    //! \reimp
    virtual QString name() const;
    virtual MAbstractInputMethod * createInputMethod(MAbstractInputMethodHost *host);
    virtual QSet<Maliit::HandlerState> supportedStates() const;
    //! \reimp_end
};
#endif // MALIITKEYBOARDPLUGIN_H
