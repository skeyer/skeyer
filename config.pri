# Configures the needed data directories for configuring lib.pri and data.pri
# Accepts the qmake variable called PREFIX

isEmpty(PREFIX) {
    PREFIX=$$OUT_PWD/install/$$TARGET
}

PREFIX = $$absolute_path($$PREFIX)

win32 {
    DATADIR = $$PREFIX/share/
    DATADIR_CPPDEFINE = "share/"
}
else: macx {
    DATADIR = $$PREFIX/share/
    DATADIR_CPPDEFINE = "share/"
}
else: android {
    DATADIR = /assets/share
    DATADIR_CPPDEFINE = "assets:/share"
}
else: unix {
    DATADIR = $$PREFIX/share/$$TARGET
    DATADIR_CPPDEFINE = $$DATADIR
}

message("Current build options for $$TARGET :")
message("    PREFIX = $$PREFIX")
message("    DATADIR = $$DATADIR")
