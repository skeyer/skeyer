include_directories(${CMAKE_SOURCE_DIR}/view)

if(CMAKE_BUILD_TYPE MATCHES Debug)
    add_definitions(-DSKEYER_VIEW_FILE=\"${CMAKE_INSTALL_PREFIX}/${SKEYER_DATA_DIR}/qml/main.qml\")
else(CMAKE_BUILD_TYPE MATCHES Debug)
    message("Building in release mode: Packing resources into " ${PROJECT_NAME} )
    qt5_add_resources(VIEW_QML_SRCS ${CMAKE_SOURCE_DIR}/view/qml.qrc)
    qt5_add_resources(VIEW_IMG_SRCS ${CMAKE_SOURCE_DIR}/view/images.qrc)

    add_definitions(-DSKEYER_VIEW_FILE=\"qrc:///qml/main.qml\")
endif(CMAKE_BUILD_TYPE MATCHES Debug)

set(VIEW_SRCS ${CMAKE_SOURCE_DIR}/view/skeyerview.cpp
              ${VIEW_QML_SRCS}
              ${VIEW_IMG_SRCS})