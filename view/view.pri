# The application data needed for Skeyer to run.
# Currently this contains keyboard definition files and dictionaries needed for each language

# Needs the variable called SKEYER_DATADIR defined already

view.files = $$PWD/language_packs

QT += gui quick qml

include(../lib/lib.pri)

# Add more folders to ship with the application, here
CONFIG(debug, debug|release){
    message("Building the application in Debug Mode.")
    message("Deploying the resources manually.")

    view.files += $$PWD/qml $$PWD/images
    view.path = $$DATADIR

    OTHER_FILES += $$PWD/qml/* $$PWD/images/*
    DEFINES += SKEYER_VIEW_FILE=\\\"$$DATADIR_CPPDEFINE/qml/main.qml\\\"

    INSTALLS += view
} else {
    message("Building the application in Release Mode.")
    message("Deploying the resources to QRC.")

    RESOURCES += $$PWD/images.qrc $$PWD/qml.qrc

    DEFINES += SKEYER_VIEW_FILE=\\\"qrc:///qml/main.qml\\\"
}

HEADERS += $$PWD/skeyerview.h
SOURCES += $$PWD/skeyerview.cpp

INCLUDEPATH += $$PWD

QML_ROOT_PATH=$$PWD/qml

