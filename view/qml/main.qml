import QtQuick 2.0

Item {
    id: screen

    width: 480
    height: 400

    KeyboardContainer
    {
        id: keyboardContainer

        x: skeyer.keyboardGeometry.x
        y: skeyer.keyboardGeometry.y
        width: skeyer.keyboardGeometry.width
        height: skeyer.keyboardGeometry.height
    }
}
