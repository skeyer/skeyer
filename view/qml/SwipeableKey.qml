import QtQuick 2.0

KeyBase {
    id: key

    property var _pointsCollected: []
    property real _cx: 0
    property real _cy: 0

    property SwipeArea mouseSource: parent
    property var relativeMousePosition: key.mapFromItem(mouseSource, mouseSource.mouseX, mouseSource.mouseY)

    keyDown: enabled && mouseSource.mousePressed && key.contains(Qt.point(relativeMousePosition.x, relativeMousePosition.y))

    property alias longPressInterval: timer.interval

    signal swipeStarted(string keyCode, var keyModel)
    signal swipeProgressed(string keyCode, var keyModel)
    signal swipeKeyRepeated(string keyCode, var keyModel)
    signal swipeEnded(string keyCode, var keyModel)

//    property real activationRadius: Math.min(width,height)

    function _angleAtTheCenter( cx, cy, points ){
        var angle = 0
        var Ax, Ay, Bx, By, Alen, Blen

        for(var i = 1; i < points.length; i++)
        {
            Ax = points[i-1].x - cx
            Ay = points[i-1].y - cy
            Bx = points[i].x - cx
            By = points[i].y - cy
            Alen = Ax*Ax + Ay*Ay
            Blen = Bx*Bx + By*By

            angle += Math.acos( (Ax*Bx + Ay*By)/(Math.sqrt( Alen*Blen )) )

            if(isNaN(angle))
                return 0
        }

        return angle
    }

    function checkLoops()
    {
        var n = _pointsCollected.length
        if(n < 7)
            return

        var angle = _angleAtTheCenter( _cx/n , _cy/n, _pointsCollected )
        var loops = Math.floor(angle/Math.PI/2)

        for(var i = 0; i < loops; i++)
            swipeKeyRepeated(keyCode, keyModel)

    }

    onRelativeMousePositionChanged:
    {
        if(!keyDown)
            return

        key._pointsCollected.push(relativeMousePosition)
        key._cx += relativeMousePosition.x
        key._cy += relativeMousePosition.y

        // Check if the key has been repeated by drawing loops


    }

    Timer {
        id:timer

        property int ticks: 0

        //TODO: make this variable so that
        //the longer the key is pressed, the faster it isrepeated()
        interval: 500
        repeat: true

        // Longpress is activated only if there is no swipe in progress
        running: key.keyDown && key.mousePressStartedHere

        onTriggered: {
            if(ticks == 0 && keyModel.length > 1) key.longPressed( keyCode, keyModel )
            else key.repeated(keyCode, keyModel)
            ticks++
        }

        onRunningChanged: if(running)ticks = 0
    }

    property bool mousePressStartedHere: false

    Connections {
        target: mouseSource
        onMousePressedChanged:
        {
            if(mouseSource.mousePressed)
            {
                 var relativeSwipeStartPosition = key.mapFromItem(mouseSource, mouseSource.mousePressStartX, mouseSource.mousePressStartY)
                 key.mousePressStartedHere = key.contains(Qt.point(relativeSwipeStartPosition.x, relativeSwipeStartPosition.y))
            }
        }
    }

    onEntered:
    {
        _cx = 0
        _cy = 0
        _pointsCollected = []
    }

    onExited:
    {
        checkLoops()

        // If the mouse is still pressed, it means there is a swipe
        if(mouseSource.mousePressed)
        {
            if(key.mousePressStartedHere) key.swipeStarted(keyCode, keyModel)
            else key.swipeProgressed(keyCode, keyModel)
        }
        else
        {
            // If the mouse started here then it could be a boring keyPress too
            if( key.mousePressStartedHere )
            {
                // - unless the key has been longPressed
                if(timer.ticks === 0)
                    key.pressed(key.keyCode, key.keyModel)
            }
            else key.swipeEnded(key.keyCode, key.keyModel)
        }

        mousePressStartedHere = false
    }

}

