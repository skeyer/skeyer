import QtQuick 2.0

Rectangle {
    id: wordlistRect
    width: 400
    height: dummyText.height*3

    color: 'black'

    property alias matches:  listView.model
    property string currentWord: ''

    signal wordSelected(string word, int index)

    Text {
        id: dummyText
        visible: false
        text: "A"
        font.pixelSize: parent.height/4
        font.bold: true
    }

    ListView {
        id: listView

        anchors.fill: parent
        orientation: ListView.Horizontal

        delegate: Item {
            width: Math.max(wordlistRect.width/5, text.width + 2*dummyText.width)
            height: wordlistRect.height

            Text {
                id: text
                anchors.centerIn: parent;
                text: word
                font.pixelSize: parent.height/4
                font.bold: wordButton.pressed
                color: wordButton.pressed ? 'white' : 'lightgrey'
            }

            MouseArea {
                id: wordButton
                anchors.fill: parent
                onClicked: wordSelected(word, index)
            }

            VerticalSeperator {
                id: verticalSeperator
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter

                seperatorColor: 'orange'
                seperatorPosition: 0.5
            }
        }

        header: Item {
            width: currentWord === "" ? 0 : Math.max( wordlistRect.width/5, currentWordText.width + 2*dummyText.width)
            height: wordlistRect.height

            Text {
                id: currentWordText
                anchors.centerIn: parent
                text: currentWord
                font.pixelSize: parent.height/4
                font.underline: true
                color: 'white'
            }

            MouseArea {
                id: currentWordButton
                anchors.fill: parent
                onClicked: wordSelected(currentWord, -1)
            }

            VerticalSeperator {
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter

                seperatorColor: 'red'
                seperatorPosition: 0.5
            }
        }
    }
}
