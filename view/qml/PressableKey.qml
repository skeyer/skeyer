import QtQuick 2.0

KeyBase {

    id: key

    keyDown: enabled && mouseArea.pressed

    Timer {
        id:timer

        property int ticks: 0

        interval: 500
        repeat: true
        running: key.keyDown

        onTriggered: {
            if(ticks == 0 && keyModel.length > 1) key.longPressed( keyCode, keyModel )
            else key.repeated(keyCode, keyModel)
            ticks++
        }

        onRunningChanged: if(running)ticks = 0
    }


    MouseArea {
        id: mouseArea
        anchors.fill: parent
        propagateComposedEvents: true
    }

    onKeyDownChanged: {
        if(!keyDown)
        {
            if(timer.ticks === 0)
                key.pressed(keyCode, keyModel)
        }
    }
}

