import QtQuick 2.0

Canvas {
    id: canvas

    antialiasing: true

    property alias mouseX: mouseArea.mouseX
    property alias mouseY: mouseArea.mouseY
    property point mousePosition: Qt.point(mouseX, mouseY)
    property alias mousePressed: mouseArea.pressed

    property real mousePressStartX: -1
    property real mousePressStartY: -1

    property bool showTrail: true


    // Useful for determining the popup size and also to normalize the inputSequence
    property int keyWidth: 40
    property int keyHeight: 40

    signal pressed()
    signal released()


    property var displayPoints: new Array(ringBufferCapacity)

    property int ringBufferCapacity: height/2
    property int ringBufferCount: 0
    property int ringBufferBack: 0

    function resetDisplayPoints()
    {
        ringBufferBack = 0
        ringBufferCount = 0
    }

    function pushDisplayPoint( displayPoint )
    {

        if(ringBufferCount < ringBufferCapacity)
        {
            displayPoints[ringBufferCount++] = displayPoint
        }
        else
        {
            displayPoints[ringBufferBack] = displayPoint
            ringBufferBack = (ringBufferBack + 1)%ringBufferCapacity
        }
    }

    onPaint: {

        var ctx = canvas.getContext("2d");
        var points = canvas.displayPoints

        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

        if(!canvas.showTrail)
            return

//        ctx.lineJoin = 'miter'
//        ctx.lineCap = 'miter'

        var p1, p2


        for (var i = 1 ; i < ringBufferCount; i++ ) {
            var progress = i/ringBufferCount
            ctx.strokeStyle = Qt.rgba(1,1,1, progress)
            p1 = points[(ringBufferBack + i-1)%ringBufferCapacity]
            p2 = points[(ringBufferBack + i)%ringBufferCapacity]

            ctx.beginPath()
            ctx.lineWidth = Math.max(progress*10, 1)

            ctx.moveTo(p1.x, p1.y);
            ctx.lineTo(p2.x, p2.y);
            ctx.stroke()
        }
    }

    onShowTrailChanged: canvas.requestPaint()

    MouseArea {
        id: mouseArea

        anchors.fill: parent

        propagateComposedEvents: true

        onPositionChanged: {
            pushDisplayPoint(Qt.point(mouse.x, mouse.y))
            canvas.requestPaint()
        }

        onPressed:
        {
            mousePressStartX = mouse.x
            mousePressStartY = mouse.y

            if(canvas.showTrail)
                pushDisplayPoint( Qt.point(mouse.x, mouse.y) )

            canvas.pressed()
        }

        onReleased:
        {
            resetDisplayPoints()

            canvas.requestPaint()
            canvas.released()
        }
    }
}
