import QtQuick 2.0

Item {
    id: popup

    width: 400
    height: 400

    property int popupKeyWidth: 40
    property int popupKeyHeight: 40

    property Item mouseSource: parent
    property bool mousePressed: false

    property int mouseX: 0
    property int mouseY: 0

    property bool raised: popup.mousePressed && popupModel.count > 0

    signal keySelected(string keyCode)

    ListModel {
        id: popupModel
        // Elements are of the type:
        // ListElement { keyCode: ''; selected: false }
    }

    function raise( keyModel, x, y ){
        if(!enabled || keyModel && keyModel.length < 1)
            return


        for(var k in keyModel ){
            if(k === 0) popupModel.append({ "keyCode": keyModel[k], "selected": true })
            else popupModel.append({ "keyCode": keyModel[k], "selected": false })
        }

        popupMenu.x = Math.min( Math.max( x - popupMenu.width/4 , 0 ) , popup.width - popupMenu.width )
        popupMenu.y = y - popupMenu.height - 5
    }

    Rectangle {
        id: overlay

        anchors.fill: parent
        color: 'black'
        opacity: raised? 0.5 : 0.0

        Behavior on opacity { NumberAnimation { duration: 200 } }
    }

    Rectangle {
        id: popupMenu

        width: popupKeyWidth*popupModel.count
        height: popupKeyHeight
        radius: 10

        visible: raised

        opacity: visible? 1.0 : 0.0
        Behavior on opacity { NumberAnimation { duration: 200 } }

        border.color: 'transparent'
        border.width: 2

        gradient: Gradient {
            GradientStop { position: 0.0; color: "white" }
            GradientStop { position: 1.0; color: "lightgrey" }
        }

        ListView {
            id: popupList
            anchors.fill: parent

            orientation: ListView.Horizontal

            model: popupModel

            delegate: Rectangle {
                id: key

                border.color: 'transparent'
                border.width: 2

                color: model.selected? 'lightgrey' : 'transparent'

                width: popupKeyWidth
                height: popupKeyHeight

                radius: index === 0 || index === popupModel.count - 1? 10 : 0

                Text {
                    anchors.centerIn: parent
                    text: model.keyCode
                    color: 'black'
//                    font.pointSize: 20
                    font.pixelSize: key.height/2
                }

                VerticalSeperator {
                    height: parent.height
                    anchors.right: parent.right
                    color: 'black'
                    seperatorPosition: 0.5
                    visible: index !== popupList.model.length - 1
                }

                Connections {
                    target: popup
                    onMouseXChanged:
                    {
                        var pos = key.mapFromItem( popup.mouseSource, mouseX, 0 )
                        if( pos.x >= 0 && pos.x <= key.width )
                            popupModel.setProperty(index, 'selected', true)
                        else popupModel.setProperty(index, 'selected', false)
                    }
                }

            }

        }
    }


    onRaisedChanged: {
        var keyCode = ''
        if(!raised){
            for(var i = 0; i < popupModel.count; i++){
                var current = popupModel.get(i)
                if( current.selected ){
                    keyCode = current.keyCode
                    break;
                }
            }

            popupModel.clear()
            popup.keySelected( keyCode )
        }
    }
}
