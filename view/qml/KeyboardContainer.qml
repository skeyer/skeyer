import QtQuick 2.0

Item {
    id: keyboardContainer

    x: 0
    y: 0
    width: 480
    height: 400

    clip: true

    property bool adjustable: skeyer.keyboardGeometryAdjustable
    property bool resizingActive: false

    BorderImage {
        id: titlebar

        source: '../images/curved_tab.png'

        anchors.top: parent.top
        anchors.left: parent.left
        width: title.x + title.width + border.right
        height: adjustable? 2*title.height : 0

        border.left: 20
        border.right: 200

        Image {
            id: closeButton

            height: title.height
            width: height

            anchors.left: parent.left
            anchors.leftMargin: width
            anchors.verticalCenter: parent.verticalCenter
            opacity: closeMouseArea.pressed? 1 : 0.75

            source: '../images/close_button.png'

            MouseArea {
                id: closeMouseArea

                cursorShape: Qt.PointingHandCursor

                anchors.fill: parent
                onClicked: Qt.quit()
            }
        }

        Text {
            id: title
            anchors.left: closeButton.right
            anchors.leftMargin: closeButton.width
            anchors.verticalCenter: parent.verticalCenter
            text: 'Skeyer'
            color: titleMouseArea.pressed? 'white' : 'grey'
            font.pointSize: 16

            MouseArea {
                id: titleMouseArea
                anchors.fill: parent
                drag.target: keyboardContainer.resizingActive? undefined : keyboardContainer

                property int resizingStartX: 0
                property int resizingStartY: 0
                property int resizingStartWidth: 0
                property int resizingStartHeight: 0

                cursorShape: keyboardContainer.resizingActive? Qt.ArrowCursor :
                             drag.active? Qt.DragMoveCursor : Qt.OpenHandCursor


                onPressAndHold: {
                    keyboardContainer.resizingActive = true
                    resizingStartX = mouse.x
                    resizingStartY = mouse.y
                    resizingStartWidth = skeyer.keyboardGeometry.width
                    resizingStartHeight = skeyer.keyboardGeometry.height
                }

                onMouseXChanged: {
                    if(!keyboardContainer.resizingActive)
                        return

                    var updatedWidth =  resizingStartWidth + (mouse.x - resizingStartX)
                    if( updatedWidth >= 0 && updatedWidth <= screen.width )
                        if(skeyer.keyboardGeometry.width !== updatedWidth)
                            skeyer.keyboardGeometry.width = updatedWidth
                }

                onMouseYChanged: {
                    if(!keyboardContainer.resizingActive)
                        return

                    var updatedHeight =  resizingStartHeight + (mouse.y - resizingStartY)
                    if( updatedHeight >= 0 && updatedHeight <= screen.height )
                        if(skeyer.keyboardGeometry.height !== updatedHeight)
                            skeyer.keyboardGeometry.height = updatedHeight
                }

                onReleased: {
                    keyboardContainer.resizingActive = false
                }
            }
        }

    }

    WordRibbon {
        id: wordRibbon
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: titlebar.bottom
        height: parent.height/(keys.rows + 2)

        matches: skeyer.wordEngine
        currentWord: skeyer.wordEngine.currentWord

        onWordSelected: skeyer.wordEngine.selectWord(word)
    }

    KeyGrid {
        id: keys

        anchors.top: wordRibbon.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        keyboardModel: skeyer.keyboardModel

        onSwipeStarted: skeyer.wordEngine.beginSwipe( keyCode )
        onSwipeEnded: skeyer.wordEngine.endSwipe( keys, points, timestamps )

        onKeyPressed: skeyer.wordEngine.acceptKeyPress( keyCode )
        onSpecialKeyPressed: skeyer.wordEngine.acceptSpecialKeyPress( keyCode )
    }

    Item {
        id: resizeHintOverlay
        anchors.fill: parent

        visible: keyboardContainer.resizingActive

        Image {
            id: rightArrow
            source: '../images/resize_hint.png'

            height: width
            width: keys.width/10

            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: width/4
            rotation: -90
        }

        Image {
            id: downArrow
            source: '../images/resize_hint.png'

            width: height
            height: keys.height/5

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: height/4
        }

        Behavior on opacity { NumberAnimation { duration: 200 } }

        Timer {
            id: blinkTimer
            interval: 500
            repeat: true
            running: keyboardContainer.resizingActive
            onTriggered: {
                if(parent.opacity === 0) parent.opacity = 1
                else parent.opacity = 0
            }
        }
    }

    Image {
        source: '../images/flare.png'
        anchors.fill: parent
        anchors.topMargin: titlebar.height
    }

    onXChanged: if(skeyer.keyboardGeometry.x !== x) skeyer.keyboardGeometry = Qt.rect(x,y,width,height)
    onYChanged: if(skeyer.keyboardGeometry.y !== y) skeyer.keyboardGeometry = Qt.rect(x,y,width,height)
    onWidthChanged: if(skeyer.keyboardGeometry.width !== width) skeyer.keyboardGeometry = Qt.rect(x,y,width,height)
    onHeightChanged: if(skeyer.keyboardGeometry.height !== height) skeyer.keyboardGeometry = Qt.rect(x,y,width,height)
}

