import QtQuick 2.0

Rectangle {
    width: 1
    height: 50

    property color seperatorColor: 'blue'
    property double seperatorPosition: 0.1

    gradient: Gradient {
        GradientStop { position: 0.05; color: "transparent" }
        GradientStop { position: seperatorPosition; color: seperatorColor }
        GradientStop { position: 0.75; color: "transparent" }
    }

}
