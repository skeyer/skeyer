import QtQuick 2.0
import QtGraphicalEffects 1.0

// Represents an individual Key.
// Listens to the keyboard's mousePositionChanged and mousePressedChanged
// It emits the signal pressed(keyCode) upon mouseEntered
//---------------------------------------------------------

Item {

    id: key
    width: 40
    height: 40

    property int rowIndex: 0
    property int columnIndex: 0

    // [ Just a list of keyCodes this key should show ]
    property var keyModel: ['D']
    property url keyFace: ''

    // Whether or not the key is allowed to show border
    property alias showBorder: highlightRectangle.showBorder

    //If the key is currently pressed or not
    property bool keyDown: false

    // Color of the key when it is glowing
    property color glowColor: 'blue'

    //***This is something like each key's unique id,
    //hence we pick the first alphabet of the unshifted key for as a key's keyCode***
    //This matters because this keyCode even represents all the subkeys it holds during a swipe
    property string keyCode: keyModel.length > 0? keyModel[0] : ''

    signal entered(string keyCode, var subKeyModel)
    signal exited(string keyCode, var subKeyModel)

    signal pressed(string keyCode, var subKeyModel)
    signal repeated(string keyCode, var subKeyModel)
    signal longPressed(string keyCode, var subKeyModel)


    onKeyDownChanged: {
        if(keyDown) entered(keyCode, keyModel)
        else exited(keyCode, keyModel)
    }


    Item {
        id: highlightRectangle

        property bool showBorder : false
        antialiasing: true

        anchors.fill: parent
        anchors.margins: 2
        clip: true

        Rectangle {
            anchors.fill: parent
            //            color: "lightblue"

            gradient: Gradient {
                GradientStop { position: 0.0; color: "transparent" }
                GradientStop { position: 0.33; color: glowColor }
                GradientStop { position: 1.0; color: "transparent" }
            }

            opacity: key.keyDown? 1.0 : 0

            Behavior on opacity { NumberAnimation { duration: 200 } }
        }


        VerticalSeperator{
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            visible: showBorder
        }

        VerticalSeperator{
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            visible: showBorder
        }
    }

    Text {
        id: text
        anchors.centerIn: parent
        text: keyModel.length > 0? keyModel[0] : ''
        visible: keyFace == ''
        color: 'white'
        font.pixelSize: key.height/3
    }

    Text {
        id: longpressText
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.margins: 5
        text: keyModel.length > 1? keyModel[1] : ''
        color: 'grey'
        font.pixelSize: key.height/4
        visible: keyFace == ''
    }

    Glow {
        //TODO: Replace me with something that will make me faster and bearable
        radius: 8
        samples: 16
        color: glowColor
        anchors.fill: text.visible? text: image
        source: text.visible? text : image
        //visible: text.visible

        fast: true
        cached: true
        spread: 0.5
//        spread: key.keyDown? 0.5 : 0.25

//        Behavior on spread { NumberAnimation { duration: 200 } }
    }


    Image {
        id              : image
        antialiasing    : true
        anchors.fill    : parent
        fillMode        : Image.PreserveAspectFit
        anchors.margins : 20
        visible         : keyFace != ""
        source          : keyFace

    }

//    RadialGradient {
//        anchors.fill: parent
//        opacity: 0.15
//        gradient: Gradient {
//            GradientStop { position: 0.0; color: glowColor }
//            GradientStop { position: key.keyDown? 0.4: 0.3; color: "transparent" }
//        }
//    }
}
