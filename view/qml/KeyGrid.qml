import QtQuick 2.0
import com.skeyer 1.0

Rectangle {
    id: keyGrid

    color: 'black'

    width: 400
    height: 500

    property var keyboardModel

    property bool swipeable: keyboardModel.swipeable
    property bool swipeActive: false

    property int rows: keyboardModel.rows
    property int columns: keyboardModel.columns

    property real keyWidth: width/columns
    property real keyHeight: height/rows

    property var swipedPoints: []
    property var swipedKeys: []
    property var swipeTimestamps: []

    signal keyPressed(string keyCode)
    signal keyRepeated(string keyCode)
    signal specialKeyPressed(int keyCode)
    signal specialKeyRepeated(int keyCode)

    signal swipeStarted(string keyCode)
    signal swipeEnded(var keys, var points, var timestamps)

    function resetInput(){
        keyGrid.swipedPoints = []
        keyGrid.swipedKeys = []
        keyGrid.swipeTimestamps = []
    }

    Repeater {
        anchors.fill: parent
        model: keyGrid.rows

        ListView {
            id: row

            property int rowIndex: index

            height: keyHeight
            width: keyWidth*keyboardModel.currentGrid.rowWidth(rowIndex)

            anchors.horizontalCenter: parent.horizontalCenter
            y: rowIndex*keyGrid.keyHeight

            model: keyboardModel.currentGrid.keyCount(rowIndex)

            orientation: ListView.Horizontal
            interactive: false

            delegate: SwipeableKey {
                id: key

                rowIndex: row.rowIndex
                columnIndex: index

                keyModel: keyboardModel.keyModelAt(rowIndex, columnIndex, keyboardModel.keyModifier, keyboardModel.layout)

                mouseSource: swipeArea

                property bool isSpaceKey: keyCode === ' '

                width: keyboardModel.currentGrid.keyWidth(rowIndex, columnIndex)*keyGrid.keyWidth
                height: keyGrid.keyHeight

                showBorder: isSpaceKey

                //Don't call it if we are already showing a popupMenu for some other key's longPress
                enabled: !popup.raised

                onPressed:
                {
                    keyPressed( keyCode, subKeyModel )
                }
                onRepeated:
                {
                    keyRepeated( keyCode, subKeyModel )
                }

                onLongPressed:
                {
                    // If a swipe hasn't started, raise the popup and collect the input
                    var pos = key.mapToItem(keyGrid, 0, 0)
                    if( !swipeActive && !popup.raised ) popup.raise(keyModel, pos.x, pos.y)
                    // else do nothing
                }

                onSwipeStarted:
                {
                    keyGrid.swipeActive = true
                    keyGrid.swipedKeys.push(keyCode)
                    keyGrid.swipeStarted(keyCode)
                }

                onSwipeKeyRepeated:
                {
                    keyGrid.swipedKeys.push(keyCode)
                }

                onSwipeProgressed:
                {
                    keyGrid.swipedKeys.push(keyCode)
                }

                onSwipeEnded:
                {
                    keyGrid.swipedKeys.push(keyCode)
                    keyGrid.swipeEnded(swipedKeys, swipedPoints, swipeTimestamps)
                }
            }
        }
    }

    SwipeArea {
        id: swipeArea

        anchors.fill: parent

        showTrail: swipeable && !popup.raised

        onMousePositionChanged:
        {
            if(swipeable)
            {
                swipedPoints.push( Qt.point(mousePosition.x/keyWidth, mousePosition.y/keyHeight) )
                swipeTimestamps.push(Date.now())
            }
        }
        onMousePressedChanged:
        {
            if(mousePressed)
            {
                if(swipeable)
                {
                    swipedPoints.push( Qt.point(mousePosition.x/keyWidth, mousePosition.y/keyHeight) )
                    swipeTimestamps.push(Date.now())
                }
            }
        }
    }

    PressableKey {
        id: layoutSwitcher
        width: keyGrid.keyWidth*2
        height: keyGrid.keyHeight
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        keyModel: ['12#']

        enabled: !swipeActive

        onPressed: specialKeyPressed(KeyboardModel.LayoutChanger)
        onRepeated: specialKeyRepeated(KeyboardModel.LayoutChanger)
    }

    PressableKey {
        id: enterKey
        width: keyGrid.keyWidth*2
        height: keyGrid.keyHeight
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        keyFace: '../images/enter.png'

        enabled: !swipeActive

        onPressed: specialKeyPressed(KeyboardModel.Enter)
        onRepeated: specialKeyRepeated(KeyboardModel.Enter)
    }

    PressableKey {
        id: shiftKey
        width: keyGrid.keyWidth*1.5
        height: keyGrid.keyHeight
        anchors.left: parent.left
        anchors.bottom: layoutSwitcher.top
        keyFace: keyboardModel.shiftState === KeyboardModel.ShiftLocked?  '../images/shift_locked.png':
                 keyboardModel.shiftState === KeyboardModel.ShiftPressed? '../images/shift_pressed.png':
                                                                          '../images/shift.png'

        enabled: !swipeActive

        onPressed: specialKeyPressed(KeyboardModel.Shift)
        onRepeated: specialKeyRepeated(KeyboardModel.Shift)
    }

    PressableKey {
        id: bkspKey
        width: keyGrid.keyWidth*1.5
        height: keyGrid.keyHeight
        anchors.right: parent.right
        anchors.bottom: enterKey.top
        keyFace: '../images/bksp.png'

        enabled: !swipeActive

        onPressed: specialKeyPressed(KeyboardModel.Backspace)
        onRepeated: specialKeyRepeated(KeyboardModel.Backspace)
    }


    PopupCharacterSelector {
        id: popup

        anchors.fill: parent

        popupKeyHeight: keyHeight*0.8
        popupKeyWidth: keyWidth

        mouseSource: swipeArea
        mousePressed: swipeArea.mousePressed
        mouseX: swipeArea.mousePosition.x
        mouseY: swipeArea.mousePosition.y

        enabled: !swipeActive

        onKeySelected: {
            if(keyCode) keyPressed( keyCode )
            resetInput()
        }
    }

    Timer {
        // This is needed for when the user takes the swipe into some place other than the keyGrid
        id: swipeEndCheckTimer
        interval: 1
        running: !swipeArea.mousePressed
        repeat: false

        onTriggered:
        {
            if(swipeActive)
                keyGrid.swipeEnded(swipedKeys, swipedPoints)
        }
    }

    onSwipeEnded:
    {
        resetInput()
        swipeActive = false
    }
}
