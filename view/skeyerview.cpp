#include "skeyerview.h"
#include "engine.h"
#include "keyboardmodel.h"
#include "settingsmanager.h"

#include <QtQml>

namespace Skeyer {

SkeyerView::SkeyerView(QWindow *parent):
    QQuickView(parent),
    m_wordEngine(new Engine(this)),
    m_keyboard(m_wordEngine->keyboardModel()),
    m_isKeyboardGeometryAdjustable(false),
    m_keyboardStretch(0),
    m_keyboardAlignment(0)
{
    registerComponents();

    setKeyboardGeometry(QRect(0,0, 480, 500));
    setKeyboardAlignment(BottomAlignment|HCenterAlignment);
    setKeyboardGeometryAdjustable(true);

    connect(this, SIGNAL(xChanged(int)), this, SLOT(updateKeyboardGeometry()));
    connect(this, SIGNAL(yChanged(int)), this, SLOT(updateKeyboardGeometry()));
    connect(this, SIGNAL(widthChanged(int)), this, SLOT(updateKeyboardGeometry()));
    connect(this, SIGNAL(heightChanged(int)), this, SLOT(updateKeyboardGeometry()));

    setColor(Qt::transparent);

    setResizeMode(SizeRootObjectToView);

    setSource(QUrl(SKEYER_VIEW_FILE));
}

SkeyerView::~SkeyerView()
{
}

QRect SkeyerView::keyboardGeometry() const
{
    return m_keyboardGeometry;
}

void SkeyerView::setKeyboardGeometry(QRect keyboardGeometry)
{
    if(m_keyboardGeometry != keyboardGeometry)
    {
        m_keyboardGeometry = keyboardGeometry;

        m_keyboardStretch = NoStretch;
        m_keyboardAlignment = NoAlignment;

        emit keyboardGeometryChanged(m_keyboardGeometry);
    }
}

bool SkeyerView::keyboardGeometryAdjustable() const
{
    return m_isKeyboardGeometryAdjustable;
}

void SkeyerView::setKeyboardGeometryAdjustable(bool adjustable)
{
    if(m_isKeyboardGeometryAdjustable != adjustable)
    {
        m_isKeyboardGeometryAdjustable = adjustable;
        emit keyboardGeometryAdjustabilityChanged();
    }
}

void SkeyerView::setKeyboardStretch(int stretch)
{
    if(m_keyboardStretch != stretch)
    {
        m_keyboardStretch = stretch;
        updateKeyboardGeometry();
    }
}

void SkeyerView::setKeyboardAlignment(int alignment)
{
    if(m_keyboardAlignment != alignment)
    {
        m_keyboardAlignment = alignment;
        updateKeyboardGeometry();
    }
}

void SkeyerView::updateKeyboardGeometry()
{
    int w = width();
    int h = height();

    int kW = (m_keyboardStretch & HorizontalStretch)? w:
             (m_keyboardStretch & HorizontalHalfStretch)? w/2:
                                                      keyboardGeometry().width();
    int kH = (m_keyboardStretch & VerticalStretch)? h:
             (m_keyboardStretch & VerticalHalfStretch)? h/2:
                                                    keyboardGeometry().height();

    int x = (m_keyboardAlignment == NoAlignment)? keyboardGeometry().x():
            (m_keyboardAlignment & LeftAlignment)?  0:
            (m_keyboardAlignment & RightAlignment)? w - kW:
                                                    (w - kW)/2;

    int y = (m_keyboardAlignment == NoAlignment)? keyboardGeometry().y():
            (m_keyboardAlignment & TopAlignment)? 0:
            (m_keyboardAlignment & BottomAlignment)? h - kH:
                                                    (h - kH)/2;


    QRect geometry = QRect(x, y, kW, kH);

    if(m_keyboardGeometry != geometry)
    {
        m_keyboardGeometry = geometry;
        emit keyboardGeometryChanged(m_keyboardGeometry);
    }
}

Engine *SkeyerView::wordEngine() const
{
    return m_wordEngine;
}

KeyboardModel *SkeyerView::keyboardModel() const
{
    return m_keyboard;
}

SettingsManager *SkeyerView::settingsManager() const
{
    return SettingsManager::instance();
}

void SkeyerView::registerComponents()
{
    qmlRegisterType<SkeyerView>("com.skeyer", 1, 0, "SkeyerView");
    qmlRegisterUncreatableType<Engine>("com.skeyer", 1, 0, "Engine","");
    qmlRegisterUncreatableType<KeyboardModel>("com.skeyer", 1, 0, "KeyboardModel", "");
    qmlRegisterUncreatableType<SettingsManager>("com.skeyer", 1, 0, "Settings","");

    qRegisterMetaType<Engine*>("Engine*");
    qRegisterMetaType<KeyboardModel*>("KeyboardModel*");
    qRegisterMetaType<SettingsManager*>("SettingsManager*");

    rootContext()->setContextProperty("skeyer", this);
}

}

