#ifndef SKEYERVIEW_H
#define SKEYERVIEW_H

#include <QQuickView>
#include <QRect>

namespace Skeyer {

class Engine;
class KeyboardModel;
class SettingsManager;

/**
 * @brief The SkeyerView Loads the skeyer view file.
 *        It also instantiates and exposes WordEngine, keyboardModel and settings to the view.
 */
class SkeyerView : public QQuickView
{
    Q_OBJECT

    Q_PROPERTY(QRect keyboardGeometry
               READ keyboardGeometry
               WRITE setKeyboardGeometry
               NOTIFY keyboardGeometryChanged)

    Q_PROPERTY(bool keyboardGeometryAdjustable
               READ keyboardGeometryAdjustable
               WRITE setKeyboardGeometryAdjustable
               NOTIFY keyboardGeometryAdjustabilityChanged)

    Q_PROPERTY(Engine* wordEngine READ wordEngine CONSTANT)
    Q_PROPERTY(KeyboardModel* keyboardModel READ keyboardModel CONSTANT)
    Q_PROPERTY(SettingsManager* settings READ settingsManager CONSTANT)

public:
    SkeyerView(QWindow* parent = 0);
    ~SkeyerView();

    /**
     * @brief The acutal contents of the keyboard are meant to be shown
     *        in a fullscreen transparent window (SkeyerView).
     *        This property specifies the bounding rectangle
     *        of the visible content(Keyboard + WordRibbon) in the window.
     * @return QRect
     */
    QRect keyboardGeometry() const;

    bool keyboardGeometryAdjustable() const;

    Engine* wordEngine() const;
    KeyboardModel* keyboardModel() const;
    SettingsManager* settingsManager() const;


    //Specifies how the keyboard expands to fill the SkeyerView
    enum KeyboardStretch {
        NoStretch = 0,
        HorizontalStretch = 1,
        HorizontalHalfStretch = 1 << 1,
        VerticalStretch = 1 << 2,
        VerticalHalfStretch = 1 << 3
    };

    //Specifies how the keyboard is aligned relative to the SkeyerView
    enum KeyboardAlignment {
        NoAlignment = 0,
        LeftAlignment = 1,
        RightAlignment = 1 << 1,
        TopAlignment = 1 << 2,
        BottomAlignment = 1 << 3,
        HCenterAlignment = 1 << 4,
        VCenterAlignment = 1 << 5
    };

public slots:
    void setKeyboardGeometry(QRect keyboardGeometry);
    void setKeyboardGeometryAdjustable(bool adjustable);

    void setKeyboardStretch(int stretch);
    void setKeyboardAlignment(int alignment);

    void updateKeyboardGeometry();

signals:
    void keyboardGeometryChanged(QRect keyboardGeometry);
    void keyboardGeometryAdjustabilityChanged();

private:
    void registerComponents();

    Engine* m_wordEngine;
    KeyboardModel* m_keyboard;

    QRect m_keyboardGeometry;
    bool m_isKeyboardGeometryAdjustable;

    int m_keyboardStretch;
    int m_keyboardAlignment;
};

}

#endif // SKEYERVIEW_H
