TEMPLATE = app
TARGET = skeyer_demo

CONFIG += c++11

include(../view/view.pri)

SOURCES += main.cpp

include(deployment.pri)
