#include <QGuiApplication>
#include <QQuickView>
#include <QQmlEngine>
#include "skeyerview.h"

using namespace Skeyer;

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    app.setOrganizationName("skeyer");
    app.setOrganizationDomain("com.skeyer");
    app.setApplicationName("skeyer");
    app.setApplicationDisplayName("Skeyer");

    SkeyerView view;

    QObject::connect(view.engine(), SIGNAL(quit()),qApp, SLOT(quit()));
    view.setKeyboardGeometryAdjustable(false);

#ifdef Q_OS_ANDROID
    view.setKeyboardStretch(SkeyerView::VerticalHalfStretch | SkeyerView::HorizontalStretch);
#else
    view.setKeyboardStretch(SkeyerView::VerticalStretch | SkeyerView::HorizontalStretch);
#endif
    view.setKeyboardAlignment(SkeyerView::BottomAlignment | SkeyerView::HCenterAlignment);
    view.show();

    return app.exec();
}
