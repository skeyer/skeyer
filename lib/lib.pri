# The library contains the skeyer's engine and it's view(along with the related files)

# Needs the variable called DATADIR defined already,
# which points to the location of the installed dictionaries and keyboard files.

QT += core

include(../resources/resources.pri)

HEADERS += $$PWD/engine.h \
    $$PWD/wordlist.h \
    $$PWD/abstractwordmatcher.h \
    $$PWD/bruteforcematcher.h \
    $$PWD/settingsmanager.h \
    $$PWD/keyboardmodel.h \
    $$PWD/toplist.h \
    $$PWD/word.h

SOURCES += $$PWD/engine.cpp \
    $$PWD/wordlist.cpp \
    $$PWD/abstractwordmatcher.cpp \
    $$PWD/bruteforcematcher.cpp \
    $$PWD/settingsmanager.cpp \
    $$PWD/keyboardmodel.cpp \
    $$PWD/word.cpp

INCLUDEPATH += $$PWD
