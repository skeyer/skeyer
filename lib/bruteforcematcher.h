#ifndef BRUTEFORCEMATCHER_H
#define BRUTEFORCEMATCHER_H

#include "abstractwordmatcher.h"
#include "wordlist.h"
#include "toplist.h"

#include <QObject>

namespace Skeyer {

class BruteForceMatcher : public AbstractWordMatcher
{
    Q_OBJECT
public:
    BruteForceMatcher(Engine* parent = 0);
    ~BruteForceMatcher();

    virtual QList<QString> matchPath(QString input,
                                     QList<QPointF> path  = QList<QPointF>(), QList<int> timestamps = QList<int>(),
                                     int count = 5 , QString context = "") override;
    QList<QString> matchText(QString input, int count = 5, QString context = "") override;

private slots:
    void onWordSelected(const QString& word, const QString &input, const QList<QPointF> &inputPath);
    void updateLocale(const QString& locale);

private:
    const qreal m_threshold;
    WordList m_wordList;
};

}
#endif // BRUTEFORCEMATCHER_H
