TEMPLATE = lib
CONFIG += c++11

include(lib.pri)

public_headers.files += $$HEADERS
public_headers.path = $$PREFIX/include

target.path =$${PREFIX}/lib

INSTALLS += public_headers target
