#include "word.h"

#include <QStringList>
#include <QVariant>
#include <qmath.h>

namespace Skeyer {

Word::Word(const QString &w, quint8 freq, quint8 originalFreq, quint16 flags):
    word(w),
    frequency(freq),
    originalFrequency(originalFreq),
    flags(flags),
    swipeHintLength(0)
{
}

Word Word::fromAttributes(const QMap<QString, QString> &attributes)
{
    Word result;

    if( attributes.contains("word") )
        result.word = attributes["word"];

    if( attributes.contains("f") )
        result.frequency = QVariant(attributes["f"]).toInt();

    if( attributes.contains("originalFreq") )
        result.frequency = QVariant(attributes["f"]).toInt();

    if(attributes.contains("flags"))
    {
        QString v = attributes["flags"];
        int flags = 0;

        if( v == "n" )
            flags = Word::n;
        else if( v == "s" )
            flags = Word::s;
        else if( v == "s" )
            flags = Word::r;
        else if( v == "r" )
            flags = Word::r;
        else if( v == "p" )
            flags = Word::p;
        else if( v == "e" )
            flags = Word::e;
        else if( v == "offensive" )
            flags = Word::offensive;
        else if( v == "babytalk" )
            flags = Word::babytalk;
        else if( v == "hand-added" )
            flags = Word::handadded;
        else if( v == "drugs" )
            flags = Word::drugs;
        else if( v == "medical" )
            flags = Word::medical;
        else if( v == "technical" )
            flags = Word::technical;
        else if( v == "abbreviation" )
            flags = Word::abbreviation;

        result.flags = flags;
    }

    return result;
}

qreal Word::probability() const
{
    if(frequency > 15) return qLn( frequency )/qLn( 255 );
    else if(originalFrequency > 15) return qLn( originalFrequency )/qLn( 255 );
    return -1;
}

void Word::incrementFrequency()
{
    if(frequency < 255)
        frequency++;
}

QDataStream &operator<<(QDataStream &out, const Word &w)
{
    out << w.word
        << w.frequency
        << w.originalFrequency
        << w.flags
        << w.swipeHintLength
        << w.swipeHintVector
        << w.wordVector;

    return out;
}

QDataStream &operator>>(QDataStream &in, Word &w)
{
    in >> w.word
       >> w.frequency
       >> w.originalFrequency
       >> w.flags
       >> w.swipeHintLength
       >> w.swipeHintVector
       >> w.wordVector;

    return in;
}

}
