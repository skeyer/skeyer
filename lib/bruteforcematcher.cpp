﻿#include "bruteforcematcher.h"
#include "settingsmanager.h"
#include "toplist.h"
#include "engine.h"

#include <QDebug>
#include <qmath.h>

namespace Skeyer {

BruteForceMatcher::BruteForceMatcher(Engine *parent):
    AbstractWordMatcher(parent),
    m_threshold(0.5)
{
    connect( m_engine, SIGNAL(wordSelected(const QString&, const QString&, const QList<QPointF>&)),
             this, SLOT(onWordSelected(const QString &, const QString &, const QList<QPointF> &)) );

    connect( SettingsManager::instance(), SIGNAL(currentLocaleChanged(const QString&)),
             this, SLOT(updateLocale(const QString&)) );

    updateLocale(SettingsManager::instance()->currentLocale());
}

BruteForceMatcher::~BruteForceMatcher()
{

}

QList<QString> BruteForceMatcher::matchPath(QString input,
                                 QList<QPointF> path, QList<int> timestamps,
                                 int count, QString context)
{
    Q_UNUSED(path)
    Q_UNUSED(context)

    input = input.toLower();

    TopList<Word, qreal> matches( count );

    qreal distance;

    // qDebug() << QString("Input = %1 ( %2 ) points").arg( input ).arg( path.length() );

    int inputLength = input.length();
    int hintLength;
    QString hint;

    int k = qMax( 5.0, m_threshold*inputLength );

    const QVector< QPair<quint8, quint8> > inputUnigramFrequency = unigramFrequencyVector(input);

    QChar lastChar = input[input.length() - 1];

//    initializeCounters();

    for( const auto c : m_keyCodeNeighbours[input[0]] ){
        const auto cursorBegin = m_wordList.begin( c );
        const auto cursorEnd = m_wordList.end( c );
        for( auto cursor = cursorBegin; cursor < cursorEnd; cursor++)
        {
            m_wordsConsidered++;
            hintLength = cursor->swipeHintLength;

            if( cursor->frequency < 15 ||
                (hintLength > 0 && qAbs(inputLength - hintLength) > k) ||
                !m_keyCodeNeighbours[ cursor->word[ cursor->word.length() - 1].toLower() ].contains( lastChar ) ||
                !unigramDistanceWithinK( inputLength, inputUnigramFrequency,
                                       hintLength, cursor->swipeHintVector, k ) )
            {
                m_wordsSkipped++;
                continue;
            }

            hint = swipeHint( cursor->word.toLower() );

            if(cursor->swipeHintVector.isEmpty())
            {
                cursor->swipeHintLength = hint.length();
                cursor->swipeHintVector = unigramFrequencyVector(hint);
            }


            distance = editDistance( input, hint, k );

            // TODO: Find a better metric for a non match of two words
            // Skipping the words which are beyond our threshold
            if(distance > k)
                continue;

            matches.add( *cursor, -distance );

            // Once we have enough words, adjust our search params, to speed things up
            // Compute the distance for such k that this can make it to toplist
            if( matches.count() == count )
                k = qMin( k, -qFloor(matches.minValue()) );

            m_wordsMatched++;
        }
    }

    TopList<QString, qreal> results(count);

    for(int i = 0; i < count; i++)
    {
        auto item = matches.at(i);
        Word w = item.first;
        qreal maxLength = qMax( (int)w.swipeHintLength, input.length() );
        qreal pEditDistance = 1.0 + item.second/maxLength;
        //TODO: Account for perplexity..
        //pTotal = pEditDistance*qPow(pDictionary, 1.0 - pEditDistance);
        qreal pTotal = pEditDistance*w.probability();

        results.add( w.word, pTotal );
    }

    //showCounters()
    return results.get();
}

QList<QString> BruteForceMatcher::matchText(QString input, int count, QString context)
{
    Q_UNUSED(context)
    input = input.toLower();

    TopList<Word, qreal> matches( count );

    qreal distance;

    int k = qMax( 2.0, m_threshold*input.length() );

    const QVector< QPair<quint8, quint8> > inputUnigramFrequency = unigramFrequencyVector(input);
    int inputLength = input.length();
    int wordLength;
    QString word;

//    initializeCounters();

    for( const auto c : m_keyCodeNeighbours[input[0]] )
    {
        const auto cursorBegin = m_wordList.begin( c );
        const auto cursorEnd = m_wordList.end( c );
        for( auto cursor = cursorBegin; cursor < cursorEnd; cursor++)
        {
            m_wordsConsidered++;

            wordLength = cursor->word.length();

            // We aren't checking the last character here because a typo can be from anywhere
            if( cursor->frequency < 15 ||
                qAbs(inputLength - wordLength) > k ||
                !unigramDistanceWithinK( inputLength, inputUnigramFrequency,
                                         wordLength, cursor->wordVector, k ))
            {
                m_wordsSkipped++;
                continue;
            }

            word = cursor->word.toLower();

            if( cursor->wordVector.isEmpty() )
                cursor->wordVector = unigramFrequencyVector(word);

            distance = editDistance( input, word, k );

            if(distance > k)
                continue;

            matches.add( *cursor, -distance );

            if( matches.count() == count )
                k = qMin( k, -qFloor(matches.minValue()) );

            m_wordsMatched++;
        }
    }

    TopList<QString, qreal> results(count);

    for(int i = 0; i < count; i++)
    {
        auto item = matches.at(i);
        Word w = item.first;
        qreal maxLength = qMax( w.word.length(), input.length() );
        qreal pEditDistance = 1 + item.second/maxLength;
        qreal pTotal = pEditDistance*w.probability();

        results.add( w.word, pTotal );
    }

//    showCounters();

    return results.get();
}

void BruteForceMatcher::onWordSelected(const QString &word, const QString &input, const QList<QPointF> &inputPath)
{
    Q_UNUSED(input)
    Q_UNUSED(inputPath)

    m_wordList.incrementProbabilityOnce(word);
}

void BruteForceMatcher::updateLocale(const QString &locale)
{
    m_wordList.loadDictionary( locale );

    // Check and Generate Cached data...
    const auto cursorBegin = m_wordList.begin();
    const auto cursorEnd = m_wordList.end();

    for( auto cursor = cursorBegin; cursor < cursorEnd; cursor++)
    {
        if(cursor->swipeHintVector.isEmpty())
        {
            if(! ( cursor->swipeHintVector.isEmpty() ||
                   cursor->wordVector.isEmpty() ) )
                continue;

            const QString& word = cursor->word.toLower();
            const QString& hint = swipeHint( word );

            cursor->wordVector = unigramFrequencyVector(word);
            cursor->swipeHintVector = unigramFrequencyVector(hint);
            cursor->swipeHintLength = hint.length();
        }
    }
}

}
