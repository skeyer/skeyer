#ifndef WORD_H
#define WORD_H

#include <QMap>
#include <QPair>
#include <QString>
#include <QVector>
#include <QDataStream>

namespace Skeyer {

class Word {
    /**
      * Represents a word
      *
      * Currently, we are generating the dictionary from the Android Latin IME, where,
      *
      * Keys = [' word', 'f', 'whitelist', '  shortcut', 'not_a_word', 'flags', 'originalFreq']
      * Flags = ['', 'hand-added', 'nonword', 'e', 'australian:nonword', 'drugs', 'medical',
      * 'nonword:offensive', 'offensive', 'babytalk', 'n', 'abbreviation', 'technical',
      * 's', 'r', 'australian', 'offensive:nonword', 'p']
      *
      *
      * f = userCount
      * originalFreq = corpusCount
      *
      * TODO: Implement support for shortcuts.
      * */
public:
    QString word; //The actual word

    quint8 frequency; // is an integer from 15 - > 255
    quint8 originalFrequency;

    quint16 flags;

    //Cached values
    quint8 swipeHintLength; //Swipe hint when this word is swiped

    QVector < QPair<quint8, quint8> > swipeHintVector;
    QVector < QPair<quint8, quint8> > wordVector;

    enum Flags {
        n = 1,
        s = 1 << 1,
        r = 1 << 2,
        p = 1 << 3,
        e = 1 << 4,
        offensive = 1 << 5,
        babytalk = 1 << 6,
        handadded = 1 << 7,
        drugs = 1 << 8,
        medical = 1 << 9,
        technical = 1 << 10,
        abbreviation = 1 << 11
    };


public:
    Word(const QString& w = "",
         quint8 freq = 16, quint8 originalFreq = 0,
         quint16 flags = 0);

    static Word fromAttributes(const QMap<QString, QString>& attributes);

    double probability() const;
    void incrementFrequency();

    inline bool operator< (const QString& other) const { return word < other; }
    inline bool operator< (const Word& other) const { return word < other.word; }

    inline bool operator> (const QString& other) const { return word > other; }
    inline bool operator> (const Word& other) const { return word > other.word; }

    inline bool operator== (const Word& other) const { return word == other.word; }

    friend QDataStream& operator<<(QDataStream &out, const Word &w);
    friend QDataStream& operator>>(QDataStream &in, Word &w);
};


QDataStream& operator<<(QDataStream &out, const Word &w);
QDataStream& operator>>(QDataStream &in, Word &w);

}

#endif // WORD_H
