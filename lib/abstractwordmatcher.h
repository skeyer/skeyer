#ifndef ABSTRACTWORDMATCHER_H
#define ABSTRACTWORDMATCHER_H

#include <QObject>
#include <QString>
#include <QPointF>
#include <QHash>
#include <QMap>

#define MAX_WORD_LENGTH 512

namespace Skeyer {

class Engine;
class KeyboardModel;
class AbstractWordMatcher : public QObject
{
    Q_OBJECT
public:
    explicit AbstractWordMatcher(Engine *parent = 0);
    virtual ~AbstractWordMatcher(){}


    /**
     * @brief Matches the given gesture path with the words in the dictionary.
     *
     * @param input - sequence of keys that are activated for this matching
     * @param path - sequence of points involved in creating the input
     * @param timestamps - timestamp in ms when each point has been registered
     * @param count - number of results to fetch. Default: 5
     * @param context - for future use for better predictions based on the last word(s) - using bi/tri/n-grams.
     * @return
     */
    virtual QList<QString> matchPath(QString input,
                                     QList<QPointF> path  = QList<QPointF>(), QList<int> timestamps = QList<int>(),
                                     int count = 5 , QString context = "");

    /**
     * @brief Matches the given text with the closest word in the dictionary.
     *
     * @param input - sequence of key presses that need to be matched
     * @param count - number of results to fetch. Default: 5
     * @param context - for future use, wrt. Predictions etc..
     * @return
     */
    virtual QList<QString> matchText(QString input, int count = 5, QString context = "");

protected:
    /**
     * @brief Returns the modified Levenstein's distance to convert word1 to word2
     *        by considering the keyboard layout.
     * @param source
     * @param destination
     * @param k - cut off distance : -1 means no cutoff distance
     * @return min(actual distance, k+1) (when k >= 0)
     */
    qreal editDistance(const QString& source, const QString& destination, int k = -1);

    qreal editDistance(const QChar* source, const int sourceLength,
                        const QChar* destination, const int destinationLength,
                        int k = -1);

    qreal pCurveDistance(const QString& source, const QList<QPointF>& path, const QString& destination);

    qreal confidence(const QString& source, const QString& destination);

    inline qreal cost(const QChar& source, const QChar& destination)
    {
        m_cost++;
        //TODO: Look if this needs to be normalized to lie in between 0 and 1.
        if(source == destination) return 0;
        if(m_keyCodeLocation.contains(source)) return m_keyCodeDistances[source].value(destination, 1);
        else return 1;
    }

    /**
     * @brief calculates a possible input a user can swipe the given word by summing all the
     * keys in between the characters of the given word.
     * @param word
     * @return
     */
    QString swipeHint(const QString& word) const;

    /**
     * @brief unigramFrequencyVector
     * @param word - the string whose frequency vector is to be computed.
     * @return "aa bbb c" -> { { keyCode('a'), 2}, {keyCode('b'), 3}, {keyCode('c'), 1} }
     */
    QVector<QPair<quint8, quint8> > unigramFrequencyVector(const QString& word);

    /**
     * @brief unigramDistanceCheck
     * @param sourceLength - length of the source string
     * @param sourceVector - unigram frequency vector of the source string
     * @param destinationLength - length of the destination string
     * @param destinationVector - unigram frequency vector of the destination string
     * @param k - cut off
     * @return false if source and destination strings can have more than k differences
     */
    bool unigramDistanceWithinK(int sourceLength, const QVector< QPair<quint8,quint8> >& sourceVector,
                              int destinationLength, const QVector< QPair<quint8, quint8> >& destinationVector,
                              int k);

signals:
    void keyboardModelUpdated();

protected:
    Engine *m_engine;
    QMap< QChar, int > m_keyCodeIndices;
    QHash< QChar, QPointF > m_keyCodeLocation;
    QHash< QChar, QString > m_keyCodeNeighbours;
    QHash< QChar, QHash < QChar, qreal > > m_keyCodeDistances;
    QHash< QChar, QHash < QChar, QString > > m_keyCodesBetween;

    int m_cost;
    int m_wordsConsidered;
    int m_wordsSkipped;
    int m_wordsMatched;

    void initializeCounters();
    void showCounters();

private slots:
    void updateKeyboardModel(const QString& locale);
private:
    KeyboardModel *m_keyboard;
};

}

#endif // ABSTRACTWORDMATCHER_H
