#ifndef KEYBOARDMODEL_TEST_H
#define KEYBOARDMODEL_TEST_H

#include <QtTest/QtTest>
#include <QString>

#include "keyboardmodel.h"

using namespace Skeyer;

class KeyboardModelTest : public QObject
{
    Q_OBJECT

    bool m_exhaustive;
    KeyboardModel keyboard;

public:
    KeyboardModelTest(bool exhaustive = false):
        m_exhaustive(exhaustive)
    {
    }

    private slots:

    void initTestCase()
    {
        QVERIFY( keyboard.loadLayout("en_US") ) ;
    }

    void defaultProperties()
    {
        QCOMPARE( keyboard.spaceKeyWidth(), 4 );

        QCOMPARE( keyboard.rows(), 4 );
        QCOMPARE( keyboard.columns(), 10 );

        QCOMPARE( keyboard.layout(), KeyboardModel::AlphabeticLayout );
        QCOMPARE( keyboard.keyModifier(), KeyboardModel::ShiftModifier );

        QCOMPARE( keyboard.shiftState(), KeyboardModel::ShiftPressed );

        QVERIFY( keyboard.swipeable() );
    }

    void getters()
    {
        QCOMPARE( keyboard.keyCodeAt(0,0), QString("Q") );
        QCOMPARE( keyboard.keyCodeAt(0,0,0), QString("Q") );
        QCOMPARE( keyboard.keyCodeAt(0,0,3), QString("") );
        QCOMPARE( keyboard.keyCodeAt(0,0,0, KeyboardModel::DefaultModifier), QString("q") );
        QCOMPARE( keyboard.keyCodeAt(0,0,0, KeyboardModel::DefaultModifier, KeyboardModel::AlphabeticLayout), QString("q") );
        QCOMPARE( keyboard.keyCodeAt(0,0,0, KeyboardModel::DefaultModifier, KeyboardModel::NumericLayout), QString("1") );
        QCOMPARE( keyboard.keyCodeAt(0,0,0, KeyboardModel::ShiftModifier, KeyboardModel::NumericLayout), QString("~") );

        QCOMPARE( keyboard.keyModelAt(0,0), (QStringList{ "Q","1" }) );
        QCOMPARE( keyboard.keyModelAt(0,0, KeyboardModel::DefaultModifier), (QStringList{ "q","1" }));
        QCOMPARE( keyboard.keyModelAt(0,0, KeyboardModel::DefaultModifier, KeyboardModel::NumericLayout), QStringList{"1"});

        QCOMPARE( keyboard.keyCodeAtPoint(0,0), QString("Q") );
        QCOMPARE( keyboard.keyCodeAtPoint(0.5,0.5), QString("Q") );
        QCOMPARE( keyboard.keyCodeAtPoint(1,0.5), QString("Q") );
        QCOMPARE( keyboard.keyCodeAtPoint(1.1,0.5), QString("W") );
        QCOMPARE( keyboard.keyCodeAtPoint(3.5,3), QString(" ") );

        QCOMPARE( keyboard.keyCodeAtPoint(0.0,2), QString("") );
        QCOMPARE( keyboard.keyCodeAtPoint(0.5,1), QString("A") );
        QCOMPARE( keyboard.keyCodeAtPoint(1.0,1), QString("A") );
        QCOMPARE( keyboard.keyCodeAtPoint(1.6,1), QString("S") );

        QCOMPARE( keyboard.keyCodesBetween("q", "p"), QString("wertyuio") );
        QCOMPARE( keyboard.keyCodesBetween("Q", "P"), QString("wertyuio") );
        QCOMPARE( keyboard.keyCodesBetween("q", "P"), QString("wertyuio") );
        QCOMPARE( keyboard.keyCodesBetween("Q", "p"), QString("wertyuio") );

        QCOMPARE( keyboard.keyCodesBetween("q", "p", KeyboardModel::ShiftModifier), QString("WERTYUIO") );
    }

    void setters()
    {
        //TODO
    }

};

#endif //KEYBOARDMODEL_TEST_H
