TEMPLATE = app
TARGET = skeyer-tests

CONFIG += c++11

QT += testlib

include(../lib/lib.pri)

HEADERS += keyboardmodel_test.h \
           engine_test.h


SOURCES += exhaustive_swipe_data.cpp \
           main.cpp

linux {
    isEmpty(target.path) {
        qnx {
            target.path = /tmp/$${TARGET}/bin
        } else {
            target.path = $${PREFIX}/bin
        }
        export(target.path)
    }
    INSTALLS += target
} else {
    target.path = $$PREFIX/
    export(target.path)
    INSTALLS += target
}
