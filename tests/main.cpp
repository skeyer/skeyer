#include "keyboardmodel_test.h"
#include "engine_test.h"

#include <QCoreApplication>
#include <QtTest/QtTest>
#include <QStringList>
#include <QDebug>
#include <QDir>

int runTest(QObject* testObject,
            QStringList arguments,
            QString reportsFormat = "txt",
            QString reportsDir = ""
            )
{

    QStringList testArguments;
    testArguments << arguments.first();
    QString testName = testObject->metaObject()->className();

    if(!reportsDir.isEmpty())
    {
        QDir d;
        d.mkpath(reportsDir);
        d.cd(reportsDir);

        QString filePath;

        if(reportsFormat.contains("xml")) filePath = d.filePath(QString("%1.xml").arg(testName));
        else filePath = d.filePath(QString("%1.txt").arg(testName));
        testArguments << "-o" << QString("%1,%2").arg( filePath ).arg(reportsFormat);
    }
    else
    {
        testArguments << QString("-%1").arg(reportsFormat);
    }

    return QTest::qExec( testObject, testArguments );
}

int main(int argc, char* argv[])
{

    QCoreApplication app(argc, argv);
    app.setOrganizationName("skeyer");
    app.setOrganizationDomain("com.skeyer");
    app.setApplicationName("skeyer");

    QStringList arguments = app.arguments();

    bool exhaustive = false;
    QString reportsFormat = "txt";
    QString reportsDir;

    if( arguments.contains("--exhaustive", Qt::CaseInsensitive) )
    {
        qDebug() << "Exhaustive set of tests requested";
        exhaustive = true;
    }

    if( arguments.contains("--reportsformat", Qt::CaseInsensitive) )
    {
        int index = arguments.indexOf("--reportsformat");
        if( index + 1 < arguments.count() )
        {
            QString format;
            format = arguments[index+1];
            if(format == "txt" || format == "xml" || format == "xunitxml" || format == "lightxml" )
            {
                reportsFormat = format;
                qDebug() << "Generating reports as" << reportsFormat;
            }
            else arguments << "--help";
        }
        else arguments << "--help";
    }

    if( arguments.contains("--reportsdir", Qt::CaseInsensitive) )
    {
        int index = arguments.indexOf("--reportsdir");
        if( index + 1 < arguments.count() )
        {
            reportsDir = arguments[index+1];
            qDebug() << "Generating reports in" << reportsDir;
        }
        else arguments << "--help";
    }

    if( arguments.contains("--help", Qt::CaseInsensitive ) || arguments.contains("-h", Qt::CaseInsensitive ) )
    {
        qDebug() << QString("Usage: %1 [options]").arg( arguments.first() );
        qDebug() << "Options:";
        qDebug() << "    --exhaustive              : Run more exhaustive versions of each testcase";
        qDebug() << "    --reportsformat <format>  : format = txt, xml, lightxml, xunitxml";
        qDebug() << "                                default: txt";
        qDebug() << "    --reportsdir <path>       : Redirect the generated reports to xunit xml files this directory";
        qDebug() << "                                default: current directory";
        qDebug() << "    --help, -h                : Show this help";
        return 0;
    }

    int status = 0;

    KeyboardModelTest keyboardModelTest(exhaustive);
    status |= runTest(&keyboardModelTest, arguments, reportsFormat, reportsDir);

    EngineTest engineTest(exhaustive);
    status |= runTest(&engineTest, arguments, reportsFormat, reportsDir);

    return status;
}
