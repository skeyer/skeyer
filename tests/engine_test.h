#ifndef ENGINE_TEST_H
#define ENGINE_TEST_H

#include <QTest>
#include <QDebug>
#include "qmath.h"

#include "engine.h"
#include "keyboardmodel.h"
#include "settingsmanager.h"
#include "toplist.h"
#include "wordlist.h"

using namespace Skeyer;

QMap< QString, QPair < QString, QList<QPointF> > > exhaustiveSwipeData();

class EngineTest : public QObject
{
    Q_OBJECT

    //We are writing our test cases for en_US
    bool m_exhaustive;
    QString m_currentLocale;
    Engine engine;

public:
    EngineTest(bool exhaustive = false, QObject* parent = 0):
        QObject(parent),
        m_exhaustive(exhaustive)
    {
    }

private slots:

    void initTestCase()
    {
        m_currentLocale = SettingsManager::instance()->currentLocale();
        SettingsManager::instance()->setCurrentLocale("en_US");
    }

    void defaultProperties()
    {
        QCOMPARE( engine.keyboardModel()->layout(), KeyboardModel::AlphabeticLayout );
        QCOMPARE( engine.keyboardModel()->rows(), 4 );
        QCOMPARE( engine.keyboardModel()->columns(), 10 );
        QVERIFY( engine.keyboardModel()->swipeable() );

        QCOMPARE( engine.currentWord() , QString() );
        QCOMPARE( engine.rowCount(), 0 );
    }

    void keyPresses()
    {
        engine.acceptKeyPress("H");
        engine.acceptKeyPress("e");
        engine.acceptKeyPress("l");
        engine.acceptKeyPress("l");
        engine.acceptKeyPress("o");
        QCOMPARE( engine.currentWord(), QString("Hello"));

        engine.selectCurrentWord();
        QCOMPARE( engine.currentWord() , QString() );
    }

    void specialKeyPresses()
    {
    }

    void swipes()
    {
        QFETCH( QString, word );
        QFETCH( QVariantList, input );
        QFETCH( QVariantList, inputPath );

        QBENCHMARK {
            engine.beginSwipe( input.first().toString() );
            engine.endSwipe( input, inputPath, QVariantList() );
            qreal accuracy = checkSuggestions(word);
            QVERIFY( accuracy > 0 );
        }
    }

    void swipes_data()
    {
        const QVariantList dummyPath{ QPointF{ 0, 0 } };

        QTest::addColumn<QString>("word");
        QTest::addColumn<QVariantList>("input");
        QTest::addColumn<QVariantList>("inputPath");

        if( m_exhaustive )
        {
            const auto testData = exhaustiveSwipeData();
            for( const auto key : testData.keys() )
            {
                const auto value = testData[key];

                if(value.first.isEmpty() ||  value.second.isEmpty())
                {
                    qDebug() << "Ignoring" << key << ":" << value;
                    continue;
                }
                QTest::newRow( QTest::toString( key ) )
                        << key
                        << toVariantList(value.first)
                        << toVariantList(value.second);
            }
        }
        else
        {
            QTest::newRow("hey")
                    << QString("hey")
                    << toVariantList("hgtrerty")
                    << dummyPath;

            QTest::newRow("beautiful")
                    << QString("beautiful")
                    << toVariantList("bvgfdrewsasdfghyuyttyuiuhgfgyuiol")
                    << dummyPath;

            QTest::newRow("global")
                    << QString("global")
                    << toVariantList("ghjkloikjnbvcfdsasdfghjkl")
                    << dummyPath;

            QTest::newRow("daughters")
                    << QString("daughters")
                    << toVariantList("dsaasdfghyuuyhghgytrereds")
                    << dummyPath;

            QTest::newRow("government")
                    << QString("government")
                    << toVariantList("ghjuiokjhbvcfdertfghbnmmnbhgfdrertghjnbhgyt")
                    << dummyPath;
        }

    }

    void cleanupTestCase()
    {
        SettingsManager::instance()->setCurrentLocale(m_currentLocale);
    }

private:

//    QList< QPair<QString, QString> > topDictionaryEntries(int n = 1000) {
//        TopList< QPair<QString, QString>, qreal > results(n);

//        WordList dictionary;
//        dictionary.loadDictionary(SettingsManager::instance()->currentLocale());
//        auto cursorBegin = dictionary.begin();
//        auto cursorEnd = dictionary.end();
//        qreal pDictionary = 0;

//        for(auto cursor = cursorBegin; cursor < cursorEnd; cursor++ )
//        {
//            if(cursor->frequency > 15) pDictionary = qLn( cursor->frequency )/qLn( 255 );
//            else if(cursor->originalFrequency > 15) pDictionary = qLn( cursor->originalFrequency )/qLn( 255 );
//            else pDictionary = 0;

//            results.add( QPair<QString,QString>(cursor->word, "") , pDictionary );
//        }

//        const auto r = results.get();
//        for(const auto i : r)
//            qDebug() << i;
//        return results.get();
//    }

    QVariantList toVariantList(const QString& word)
    {
        QVariantList result;

        for(const auto c : word)
            result << c;
        return result;
    }

    QVariantList toVariantList(const QList<QPointF>& points)
    {
        QVariantList result;

        for(const auto p : points)
            result << p;

        return result;
    }

    qreal checkSuggestions(QString word)
    {
        qreal accuracy = 0;
        word = word.toLower();

        if(engine.currentWord().toLower() == word)
        {
            accuracy = 1;
        }
        else
        {
            for(int i = 0; i < engine.rowCount(); i++)
            {
                if(engine.data( engine.index(i) ).toString().toLower() == word )
                {
                    int total = (engine.rowCount() + 1); // +1 for the currentWord
                    accuracy = (total - (i + 1))/total;
                    break;
                }
            }
        }

        return accuracy;
    }

};

#endif // ENGINE_TEST_H
